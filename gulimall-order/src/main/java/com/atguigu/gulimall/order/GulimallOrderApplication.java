package com.atguigu.gulimall.order;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;


/**
 * @Author 使用rabbitmq
 * 1.引入amqp pom文件，RabbitAutoConfiguration自动生效
 * 3. 配置文件配置spring.rabbitmq信息
 * 4.@EableRabbit 开启功能
 * 5.监听消息 @RabbitListener
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableRabbit
@RabbitListener
public class GulimallOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimallOrderApplication.class, args);
    }

}
