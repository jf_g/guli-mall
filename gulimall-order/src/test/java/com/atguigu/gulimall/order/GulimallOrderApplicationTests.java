package com.atguigu.gulimall.order;


import com.atguigu.gulimall.order.entity.OrderSettingEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GulimallOrderApplicationTests {
    @Autowired
    AmqpAdmin amqpAdmin;
    @Autowired
    RabbitTemplate rabbitTemplate;

    /**
     * 测试rabbitmq
     * 1.使用AmqpAdmin创建
     */
    @Test
    public void createExchange() {
        DirectExchange directExchange = new DirectExchange("hello-exchange", true, false);
        amqpAdmin.declareExchange(directExchange);
    }

    @Test
    public void createQueue() {
        Queue queue = new Queue("hello-queue", true);
        String queue1 = amqpAdmin.declareQueue(queue);
        System.out.println(queue1);
    }

    @Test
    public void createBinding() {
        Binding binding = new Binding("hello-queue", Binding.DestinationType.QUEUE, "hello-exchange", "hello-queue-key", null);
        amqpAdmin.declareBinding(binding);
    }

    @Test
    public void sendMsg() {
        OrderSettingEntity orderSettingEntity = new OrderSettingEntity();
        orderSettingEntity.setId(1L);
        String msg = "hello world";
        rabbitTemplate.convertAndSend("hello-exchange", "hello-queue-key", orderSettingEntity);

    }
}
