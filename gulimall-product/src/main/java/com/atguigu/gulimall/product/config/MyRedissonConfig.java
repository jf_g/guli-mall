/*
package com.atguigu.gulimall.product.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;

import java.io.IOException;

*/
/**
 * @author JianFeiGan
 * @version 1.0
 * @URL jf-server.top
 * @date 2021/4/17 13:03
 *//*

public class MyRedissonConfig {

    @Bean(destroyMethod = "shutdown")
    RedissonClient redisson() throws IOException {
        Config config = new Config();
        config.useClusterServers()
                .addNodeAddress("redis://192.168.69.130:7004", "redis://192.168.69.130:7001");
        return Redisson.create(config);
    }
}
*/
